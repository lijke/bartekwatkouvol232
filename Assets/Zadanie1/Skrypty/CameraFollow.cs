﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Camera camera;
    public bool followPlayer;
    public Transform playerTransform;

    private void Update()
    {
        if(followPlayer)
            camera.transform.position = new Vector3( playerTransform.position.x,playerTransform.position.y,-10);
    }
}
