﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingBall : MonoBehaviour
{
    private bool isPressed;
    [SerializeField] private Rigidbody2D rigidbody2d;
    [SerializeField] private SpringJoint2D springJoint2d;
    private float releaseDelay;
    public CameraFollow cameraFollow;
   
    private void Start()
    {
        releaseDelay = 1 / (springJoint2d.frequency * 4);
        cameraFollow = GameObject.Find("Main Camera").GetComponent<CameraFollow>();
        rigidbody2d.isKinematic = true;

    }
    private void Update()
    {
        if(isPressed)
        {
            MoveObject();
        }
    }
    private void OnMouseDown()
    {
        isPressed = true;
        rigidbody2d.isKinematic = true;
     
    }
    private void OnMouseUp()
    {
        isPressed = false;
        rigidbody2d.isKinematic = false;
        cameraFollow.followPlayer = true;
        StartCoroutine(DropBall()); 

    }
    private void MoveObject()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        rigidbody2d.position = mousePos;
    }
    private IEnumerator DropBall()
    {
        yield return new WaitForSeconds(releaseDelay);
        springJoint2d.enabled = false;

    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.CompareTag("Ground"))
        {
            cameraFollow.followPlayer = false;
            GameManagerScene1.instance.respawnNextObject();
        }
    }
}
