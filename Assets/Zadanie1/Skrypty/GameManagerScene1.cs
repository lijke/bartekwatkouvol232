﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManagerScene1 : MonoBehaviour
{
    public GameObject prefab;
    public Transform spawnPosition;
    public static GameManagerScene1 instance;
    public Camera camera;
    private void Awake()
    {
        instance = this;
    }
    public void respawnNextObject()
    {
       var objToSpawn= Instantiate(prefab, spawnPosition);
        camera.transform.position = new Vector3(objToSpawn.transform.position.x, objToSpawn.transform.position.y, -10);
        camera.GetComponent<CameraFollow>().playerTransform = objToSpawn.transform;
        
    }
}
