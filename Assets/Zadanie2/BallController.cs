﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public CircleCollider2D circleCollider2D;
    public Rigidbody2D rigidbody2d;
    public List<GameObject> target;
    private void Start()
    {
        circleCollider2D = GetComponent<CircleCollider2D>();
        rigidbody2d = GetComponent<Rigidbody2D>();
    }
    private void OnDestroy()
    {
        Spawner.instance.counter--;
    }

    private void Update()
    {
        if (transform.localScale.x > 50)
        {
            if (Spawner.instance.counter < 250)
            {

                Spawner.instance.SpawnFromBigBall((int)rigidbody2d.mass);
                Destroy(gameObject);
            }
            else
                circleCollider2D.isTrigger = false;

        }
        else
        {
            foreach (var item in target)
            {
                if (item != null)
                {
                    item.transform.position = Vector2.MoveTowards(item.transform.position, transform.position, 0.003f);
                    if (Vector2.Distance(item.transform.position, transform.position) < 2)
                    {

                        transform.localScale += item.transform.localScale;
                        circleCollider2D.radius += 0.05f;
                        Destroy(item);
                    }
                }

            }
        }
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            target.Add(collision.gameObject);
        }
    }


            
        
    
}
