﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Spawner : MonoBehaviour
{
    public GameObject prefab;
    public int counter;
    public Transform maxY;
    public Transform minY;
    public Transform maxX;
    public Transform minX;
    private Vector2 transformToSpawn;
    public Text UiCounterBalls;
    public static Spawner instance;
    private void Awake()
    {
        instance = this;
    }
    private void Start()
    {
        InvokeRepeating("Spawn", 0f,0.25f);
    }
    private void Update()
    {
        UiCounterBalls.text = counter.ToString();
    }
    public void Spawn()
    {
        if (counter < 250)
        {

            Instantiate(prefab, CalculatePosition(), Quaternion.identity);
            counter++;
        }
     

    }
    public void SpawnFromBigBall(int count)
    {
        for (int i = 0; i < count; i++)
        {
            var obj=Instantiate(prefab, CalculatePosition(), Quaternion.identity);
            obj.GetComponent<Rigidbody2D>().AddForce(CalculatePosition(), ForceMode2D.Impulse);
            counter++;
        }
    }
    public Vector2 CalculatePosition()
    {
        var x = Random.Range(minX.position.x, maxX.position.x);
        var y = Random.Range(minY.position.y, maxY.position.y);
        return new Vector2(x, y);
    }
}
